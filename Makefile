DESTDIR ?= /
CC=gcc
CFLAGS = -DPRPLED_HA -I./libi2c/include
LDFLAGS = -L./libi2c/lib
LDLIBS = -lpthread -li2c

.PHONY: all libi2c

all: libi2c
	@$(CC) $(CFLAGS) $(LDFLAGS) mxhsrprpd.c mxhsrprp.c -o mxhsrprpd $(LDLIBS)
	@$(CC) mxprpinfo.c -o mxprpinfo
	@$(CC) prpsuper.c -o mxprpsuper

libi2c:
	$(MAKE) -C libi2c

install: all
	$(MAKE) -C libi2c install
	ldconfig
	mkdir -p $(DESTDIR)/usr/sbin/
	cp mx_hsrprp $(DESTDIR)/usr/sbin
	cp mxhsrprpd $(DESTDIR)/usr/sbin
	cp mxprpinfo $(DESTDIR)/usr/sbin
	cp mxprpsuper $(DESTDIR)/usr/sbin

clean:
	$(MAKE) -C libi2c clean
	@rm -f mxhsrprpd
	@rm -f mxprpinfo
	@rm -f mxprpsuper

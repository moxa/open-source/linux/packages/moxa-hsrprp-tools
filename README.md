# moxa-hsrprp-tools

MOXA HSR/PRP card utility base on SMBUS to query FPGA related register.

1. Compiler and install Moxa HSR/PRP card utility
```
# make install
mx_hsrprp	- Script for systemd service
mxhsrprpd	- Configure hsr/prp mode, collect Ethernet counters, link status, link speed
mxprpsuper	- Send PRP/HSR supervison frame.
mxprpinfo	- Get current hsr/prp mode, collect Ethernet counters, link status, link speed. Depends on mxhsrprpd
mxprpalarm	- mxhsrprpd will execute this when link status change. This script can be modify by customer.
```

Usage for `mxhsrprpd`:
```
The time sync daemon default configure wtih
  -h: Show this information.
  -B: Run daemon in the background
  -b: SMBUS device, default is /dev/i2c-0
  -t: HSR/PRP Status update period. Default is 3 second.
  -m: configure to prp or hsr mode, default is prp mode.
          The argurement is [index]:[mode]
          [index] range from 0~7.
          [mode] 0 is prp, mode 1 is hsr.
          Ex: Set card 0 to hsr mode, card 1 to prp mode.
          root@Moxa:~# mxhsrprpd -t 2 -m 0:1,1:0
  -s: configure fiber speed, default is auto detect mode.
          The argurement is [index]:[speed]
          [index] range from 0~7.
          [speed] 0 is 100M, 1 is 1000M. (default fiber speed is 1000M)
          Ex: Set card 0 fiber speed to 100M, card 1 fiber speed to 1000M.
          root@Moxa:~# mxhsrprpd -t 2 -s 0:0,1:1
```

2. Verify main board SMBUS host driver is exist. The following command is Intel x86 platform
```
lsmod | grep i2c_i801
```

3. Troubleshooting for i2c_i801 driver.
```
a. for DA-720/DA682B, the driver is built-in kernel 3.19 or later
   for DA-820, the driver is built-in kernel 3.0 or later
          for DA-820C, the driver is built-in kernel 4.0 or later
b. ACPI conflict with SMBUS issue
   1) edit /etc/default/grub and insert acpi_enforce_resources=lax into the parameter string of GRUB_CMDLINE_LINUX,
   e. g. GRUB_CMDLINE_LINUX='acpi_enforce_resources=lax'
   2) then run update-grub and reboot.
```

4. Verify I2C-dev bus number, the following result is Intel x86 platform.
```
# cat /sys/class/i2c-dev/i2c-0/name
SMBus I801 adapter at b000
```

5. Launch the mxhsrprpd manually
```
# mxhsrprpd -t 2
```

6. Run the mxhsrprpd automatically at booting
```
# For Debian 7 system:

root@Moxa:/home/mxhsrprp# cp -a fakeroot/etc/init.d/mx_prp.sh /etc/init.d/
root@Moxa:/home/mxhsrprp# insserv -d mx_hsrprp

# For Ubuntu or Debian 8/9/later system (see 8. Setup systemd service):

root@Moxa:/home/mxhsrprp# systemctl daemon-reload && systemctl start mx_hsrprp.service

# For Redhat Enterprise:

root@Moxa:/home/mxhsrprp# chkconfig --levels 2345 mx_hsrprp on
```

7. Setup systemd service

to edit `/lib/systemd/system/mx_hsrprp.service`

```
[Unit]
Description=Moxa HSR-PRP daemon service

[Service]
Type=oneshot
ExecStart=/usr/sbin/mx_hsrprp start
ExecStop=/usr/sbin/mx_hsrprp stop
RemainAfterExit=yes

[Install]
WantedBy=multi-user.target
```

then enable service:

```
systemctl enable mx_hsrprp.service
```
